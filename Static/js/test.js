$(document).ready(function () {


    $('#addBookForm').submit(function (e) {

        $.ajax({
            type: 'POST',
            url: '../books/addBook',
            data: $('#addBookForm').serialize(),
            success: function (data) {

                data = JSON.parse(data);
                console.log(data);
                $('#ajaxResult').text(data.message);
                $('#ajaxResult').css('display','inline-block');
            },
            failure: function () {
            // обработка ошибки
            }
        });


        return false;
    });

});