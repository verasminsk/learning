<?php
namespace TestMVC\Application\Controllers;

class AboutController extends \TestMVC\Core\Controller
{
    public function indexAction() {
        $this->template->render();
    }

    public function contactAction() {
        $this->template->render();
    }
}