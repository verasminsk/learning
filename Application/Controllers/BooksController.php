<?php
namespace TestMVC\Application\Controllers;

use TestMVC\Application\Models\Books;

class BooksController extends \TestMVC\Core\Controller
{

    public function createAction() {
        $this->template->render();
    }

    public function addBookAction()
    {
        $booksadd = new Books();
        $booksadd->addBook($_REQUEST['name'],$_REQUEST['author'],$_REQUEST['description']);

        if ($this->isAjax()){

            $response = new \stdClass();
            $response->message="ok";
            $response->code = 200;

            echo json_encode($response);
            exit();

            $this->template->render(false);
        } else {
            $this->template->render();
        }

    }

    public function addgenreAction() {
        $this->template->render();
    }

    public function viewAction() {
        $this->template->render();
    }

}