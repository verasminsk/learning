<?php
/**
 * Created by PhpStorm.
 * User: Vitali_Tsyrkin
 * Date: 22-Mar-16
 * Time: 19:26
 */

namespace TestMVC\Application\Controllers;

class ErrorController extends \TestMVC\Core\Controller
{
    public function error404Action()
    {
        echo 'PAGE NOT FOUND';
        die();
    }

    public function handleError()
    {
       var_dump(func_get_args());
    }
}