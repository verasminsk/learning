<?php
namespace TestMVC\Application\Controllers;

use TestMVC\Application\Models\Books;

class MainController extends \TestMVC\Core\Controller
{
    public function indexAction(){
        $books = new Books();
        $books = $books->getAllBooks();
        $this->template->setVarInTemplate('books', $books);
        $this->template->render();
    }
}