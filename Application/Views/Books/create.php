<script src="/Static/js/test.js"></script>
<div class="row">
    <div class="col-md-8">
        <h1 class="page-header">
            add book
        </h1>
        <div id="ajaxResult" style="display:none"></div>
        <div>
            <form id="addBookForm" name="addBook" method="GET" action="../books/addbook">
                <input name="action" type="hidden" value="add">
                <div>
                    <input type="text" name="name" size="22" placeholder="*Название">
                    <input type="text" name="author" size="22" placeholder="*Автор">
                    <input type="text" name="description" size="23" placeholder="Описание">
                    <select name="genre">
                        <?php
                        // список жанров из таблицы жанров
                        ?>
                        <option value="genre1">жанр1</option>
                        <option value="genre2">жанр2</option>
                        <option value="genre3" selected="selected">жанр3</option>
                    </select>                </div>
                <input type="submit" value="Добавить Книгу">
                <input type="reset" value="Очистить">
            </form>
        </div>
        <h1 class="page-header">
            add genre
        </h1>
        <div>
            <form name="addGenre" method="GET" action="../books/addgenre">
                <input name="action" type="hidden" value="addj">
                <div>
                    <input type="text" name="genre" size="22" placeholder="*Жанр">
                </div>
                <input type="submit" value="Добавить Жанр">
                <input type="reset" value="Очистить">
            </form>
        </div>
    </div>


