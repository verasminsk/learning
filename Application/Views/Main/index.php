<div class="row">

    <!-- Blog Entries Column -->
    <div class="col-md-8">

        <a href="/books/create">Add book & genre</a>

        <h1 class="page-header">
            Books list
        </h1>

        <!-- First Blog Post -->

        <?php foreach ($books as $books): ?>
            <h2>
                <a href="#">Book: <?php echo $books['name'] ?></a>
            </h2>
            <p><span class="glyphicon glyphicon-time"></span> <?php echo $books['date'] ?></p>
            <hr>
            <p>book: <?php echo $books['name'] ?></p>
            <?php if (isset($books['author'])) { ?><p>author: <?php echo $books['author'] ?></p><?php }?>
            <?php if (isset($books['description'])) { ?><p>description: <?php echo substr($books['description'],0,125).'...' ?></p><?php }?>
            <a class="btn btn-primary" href="/books/view">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
            <hr>
        <?php endforeach; ?>

        <!-- Pager -->
        <ul class="pager">
            <li class="previous">
                <a href="#">&larr; Older</a>
            </li>
            <li class="next">
                <a href="#">Newer &rarr;</a>
            </li>
        </ul>

    </div>
