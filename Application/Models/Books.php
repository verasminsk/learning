<?php
namespace TestMVC\Application\Models;

class Books extends \TestMVC\Core\Model
{
    public function getAllBooks()
    {
        return $this->db->query('select * from books_catalog')->fetchAll();
    }

    public function addBook($name, $author, $description)
    {
        return $this->db->query('INSERT INTO books_catalog(name, author, description) VALUES (:name, :author, :descr);',
            [':name' => $name, ':author' => $author, ':descr' => $description]);

    }

}