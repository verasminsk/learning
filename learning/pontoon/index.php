<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="css/pontoon.css"/>
    <meta name="author" content="VerAS">
    <meta name="description" content="игра в Двадцать одно">
    <meta name="keywords" content="игра играть двадцать одно 21 очко">
    <?php
    if (!isset($_GET['action']) || ($_GET['action'] == 'play')) {
        $title = 'Игра в двадцать одно.';
    } elseif ($_GET['action'] == 'rules') {
        $title = 'Правила игры в двадцать одно.';
    } elseif ($_GET['action'] == 'result') {
        $title = 'Результаы игры в двадцать одно.';
    } else {
        $title = 'Игра в двадцать одно';
    }
    echo '<title>' . $title . '</title>' . "\n";
    ?>
</head>
<body>
<div id="content">
    <?php
    if (!isset($_GET['action'])) {
        // стартовая страница
        echo '<div class="header">Здесь Вы можете сыграть в игру "Двадцать одно".</div>' . "\n";
        echo '<div class="butt"><a href="?action=play" title="играть">Играть</a></div>' . "\n";
        echo '<div class="butt"><a href="?action=stat" title="статистика иры">Статистика игры</a></div>' . "\n";
        echo '<div class="butt"><a href="?action=rules" title="правила игры в двадцать одно">Правила игры</a></div>' . "\n";
    } elseif ($_GET['action'] == 'play') {
        // игра
        echo '<div class="header">Игра в двадцать одно.</div>' . "\n";

        if (!isset($_GET['koloda'])) {
            // первая раздача
            $play1sum = 0;
            $play2sum = 0;
            $play1 = array();
            $play2 = array();
            // создаем массив из колоды в 36 карт
            // p-пики, k-крести, b-бубны, с-черви
            $masty = array(0 => 'p', 'k', 'b', 'c');
            //$masty = array(0 => '&#9824;', '&#9827;', '&#9830;', '&#9829;');
            $koloda = array();
            foreach ($masty as $m) {
                for ($k = 6; $k <= 10; $k++) {
                    // карты от 6 до 10
                    $koloda[$m . $k] = $k;
                }
                $koloda[$m . 'V'] = 2;
                $koloda[$m . 'D'] = 3;
                $koloda[$m . 'K'] = 4;
                $koloda[$m . 'T'] = 11;
            }
            /*echo '<pre>карты в колоде: ';
            print_r($koloda);
            echo '</pre></br>';*/

            // первая сдача по 2 карты
            // сдаем рандом карты первому игроку
            $play1 = array_rand($koloda, 2);
            $play1sum = $play1sum + $koloda[$play1[0]] + $koloda[$play1[1]];
            unset($koloda[$play1[0]], $koloda[$play1[1]]); // удаляем из колоды
            // сдаем рандом карты второму игроку
            $play2 = array_rand($koloda, 2);
            $play2sum = $play2sum + $koloda[$play2[0]] + $koloda[$play2[1]];
            unset($koloda[$play2[0]], $koloda[$play2[1]]); // удаляем из колоды
            // проверка суммы карт
            chekPlaySum($play1sum, '1', count($play1));
            chekPlaySum($play2sum, '2', count($play2));

        } else {
            // продолжение игры
            $koloda = unserialize($_GET['koloda']);
            $play1sum = $_GET['play1sum'];
            $play2sum = $_GET['play2sum'];
            $play1 = unserialize($_GET['play1']);
            $play2 = unserialize($_GET['play2']);
            $goPlay = $_GET['goPlay'];

            // последующие раздачи по 1 карте
            if ($goPlay == 1) {
                // сдаем рандом карту первому игроку
                $play1[] = array_rand($koloda, 1);
                $play1sum = $play1sum + $koloda[$play1[count($play1) - 1]];
                unset($koloda[$play1]);  // удаляем из колоды
            } elseif ($goPlay == 2) {
                // сдаем рандом карту второум игроку
                $play2[] = array_rand($koloda, 1);
                $play2sum = $play2sum + $koloda[$play2[count($play2) - 1]];
                unset($koloda[$play2]); // удаляем из колоды
            }
            // проверка суммы карт
            chekPlaySum($play1sum, '1', count($play1));
            chekPlaySum($play2sum, '2', count($play2));
        }
        if (($play1sum >= 21) || ($play2sum >= 21)) {
            // игра закончена       
            viewKardPlayer($play1, '1', $play1sum);
            viewKardPlayer($play2, '2', $play2sum);
            // обнуляем крты
            $koloda = NULL;
            echo '<div class="butt"><a href="?action=play" title="играть">Играть ещё.</a></div>' . "\n";
            echo '<div class="butt"><a href="?action=stat" title="статистика иры">Статистика игры</a></div>' . "\n";
        } else {
            // продолжаем игру
            viewKardPlayer($play1, '1', $play1sum);
            viewKardPlayer($play2, '2', $play2sum);
            echo '<div id="control">';
            formAddcard($koloda, $play1sum, $play2sum, $play1, $play2, '1');
            formAddcard($koloda, $play1sum, $play2sum, $play1, $play2, '2');
            echo '<div class="butt"><a href="?action=rules" title="правила игры в двадцать одно">Правила игры</a></div>' . "\n";
            echo '</div>';
        }
    } elseif ($_GET['action'] == 'rules') {
        // описание правил игры
        echo '<div class="header">Описание правил Игры в двадцать одно.</div>' . "\n";
        require_once('rules.php'); // правила во внешнем файле
    } elseif ($_GET['action'] == 'stat') {
        // вывод статистики результатов игры
        echo '<div class="header">Таблица результатов игры в двадцать одно.</div>' . "\n";
        // из базы вывести сводную таблицу: общее ко-во игр, статистику побед и поражений игроков + среднее кол-во очков при поражении
        
        
        $db = connectDB(); // подключаемся к базе

        // записываем в базу
        //$rows = $db->exec('INSERT INTO pontoon(player, victory, losing, score) VALUES ("' . $player . '", "' . $victory . '", "' . $losing . '", "' . $playSum . '");');

        // в случае ошибки SQL выражения выведем сообщене об ошибке
        $error_array = $db->errorInfo();
        if ($db->errorCode() != 0000) {
            echo "SQL ошибка: " . $error_array[2] . '<br />';
        }
        
        echo '<div>почти готово :)</div>' . "\n";
    } else {
        // хм.... левый action
        echo '<div class="errorText">' . "\n";
        echo '<div class="header">Таки добрый дзень, кулхацкер...</div>' . "\n";
        echo '</div>' . "\n";
        echo '<div class="butt"><a href="?action=play" title="играть">Играть</a></div>' . "\n";
        echo '<div class="butt"><a href="?action=stat" title="статистика иры">Статистика игры</a></div>' . "\n";
        echo '<div class="butt"><a href="?action=rules" title="правила игры в двадцать одно">Правила игры</a></div>' . "\n";
    }
    ?>
</div>
</body>
</html>
<?php

function viewKardPlayer($card, $payer, $playSum)
{
    echo '<div id="player' . $payer . '">' . "\n";
    echo '<div>карты ' . $payer . ' игрока: </div>' . "\n";
    $kk = 0;
    foreach ($card as $ct) {
        $kk++;
        echo '<div class="card' . ($kk) . '">';
        if (substr($ct, 0, 1) == 'p') {
            echo '<div class="piki">' . mb_substr($ct, 1) . ' &#9824;</div>';
        } elseif (substr($ct, 0, 1) == 'k') {
            echo '<div class="kresti">' . mb_substr($ct, 1) . ' &#9827;</div>';
        } elseif (substr($ct, 0, 1) == 'b') {
            echo '<div class="bubni">' . mb_substr($ct, 1) . ' &#9830;</div>';
        } elseif (substr($ct, 0, 1) == 'c') {
            echo '<div class="cervi">' . mb_substr($ct, 1) . ' &#9829;</div>';
        }
        echo '</div>' . "\n";;
    }
    echo '<div class="sumCard">сумма карт ' . $payer . ' игрока: ' . $playSum . "</div>\n";
    echo '</div>' . "\n";
}

function chekPlaySum($playSum, $player, $cKart)
{
    if (($playSum == 22) && ($cKart == 2)) {
        echo '<div class="victory">Победа ' . $player . ' игрока - Золотое ОЧКО!!!</div>' . "\n";
        saveDBrz($playSum, $player, '1', '0');
    } elseif ($playSum == 21) {
        echo '<div class="victory">Победа ' . $player . ' игрока - 21-но !!!</div>' . "\n";
        saveDBrz($playSum, $player, '1', '0');
    } elseif ($playSum > 21) {
        echo '<div class="losing">' . $player . ' игрок ПРОИГРАЛ!!!</div>' . "\n";
        saveDBrz($playSum, $player, '0', '1');
    }
}

function formAddcard($koloda, $play1sum, $play2sum, $play1, $play2, $player)
{
    if ($player == 1) {
        $napr = '<-';
    } else {
        $napr = '->';
    }
    echo '<form name="addKart' . $player . '" method="GET" action="?">' . "\n";
    echo '       <input name="action" type="hidden" value="play">' . "\n";
    echo '       <input name="goPlay" type="hidden" value="' . $player . '">' . "\n";
    echo '       <input name="koloda" type="hidden" value=\'' . serialize($koloda) . '\'>' . "\n";
    echo '       <input name="play1sum" type="hidden" value="' . $play1sum . '">' . "\n";
    echo '       <input name="play2sum" type="hidden" value="' . $play2sum . '">' . "\n";
    echo '       <input name="play1" type="hidden" value=\'' . serialize($play1) . '\'>' . "\n";
    echo '       <input name="play2" type="hidden" value=\'' . serialize($play2) . '\'>' . "\n";
    echo '<input type="submit" value="' . $napr . ' Еще карту ' . $player . '-му?">' . "\n";
    echo '</form>' . "\n";
}


function saveDBrz($playSum, $player, $victory, $losing)
{
// сохраняем результат игры в базу данных
    // $host = 'localhost'; // имя хоста
    // $database = 'db_pontoon'; // имя базы данных
    // $user = 'pontoon'; // заданное вами имя пользователя
    // $pswd = 'pontoon'; // заданный вами пароль
    // $dbh = mysql_connect($host, $user, $pswd) or die("Не могу соединиться с MySQL.");
    // mysql_select_db($database) or die("Не могу подключиться к базе.");
    //$sql = 'INSERT INTO pontoon SET player = ' . $player . ', victory = ' . $victory . ', losing = ' . $losing . ', score = ' . $playSum . ';';
    //$okdb = mysql_query($sql);
    //if (!$okdb) {
    //    echo 'ошибка сохранения результата...';
    //}
    //mysql_close($dbh);
    
        $db = connectDB(); // подключаемся к базе

        // записываем в базу
        $rows = $db->exec('INSERT INTO pontoon(player, victory, losing, score) VALUES ("' . $player . '", "' . $victory . '", "' . $losing . '", "' . $playSum . '");');

        // в случае ошибки SQL выражения выведем сообщене об ошибке
        $error_array = $db->errorInfo();
        if ($db->errorCode() != 0000) {
            echo "SQL ошибка: " . $error_array[2] . '<br />';
        }
}

function connectDB()
{
    $db = new PDO('mysql:host=localhost;dbname=db_pontoon;charset=UTF8', 'pontoon', 'pontoon',
        array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET \'utf8\'',
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SESSION collation_connection = \'utf8_general_ci\''));
    return $db;
}
?>