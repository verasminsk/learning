<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="css/learners.css" />
    <meta name="author" content="VrAS">
    <meta name="description" content="база/список учащихся php курса">
    <meta name="keywords" content="список база учащийся ученик php курс имя телефон емалй скайп">
<?php
if (!isset($_GET['nv']) && $_GET['action'] == 'view') {
    $title='список учащихся php курса';
} elseif (isset($_GET['nv'])) {
    $title='данные по учащемуся';
} elseif ($_GET['action'] == 'add') {
    $title='добавление учащегося';
} elseif ($_GET['action'] == 'del') {
    $title='удаление учащегося';
} elseif ($_GET['action'] == 'edit') {
    $title='редактирование данных участника';
}
echo '    <title>'.$title.'</title>';
?>

</head>
<body>
<div id="content">
<div class="header">Программирование на PHP. Разработка WEB-приложений.</div>
<?php
if ((empty($_GET['action'])) || (($_GET['action'] == 'view') && (!isset($_GET['nv'])))) {
    // нет значения action или оно равно view и нет значения nv
    if (!file_exists('learners.dat')) {
        // фйла/базы нету, создаём и проверяем права на файловую систему
        $fData = fopen('learners.dat', "w+");
        $testData = 'testDB';
        if (fwrite($fData, serialize($testData)) == FALSE) {
            echo 'Err: ошибка записи файла/базы'. "\n";
            die; // убиваем скрипт
        }
        fclose($fData);
    }
    // форма добавления элемента в базу
    ?>
<div id="formAdd">
    <form name="addLearnes" method="GET" action="learners_php.php">
        <input name="action" type="hidden" value="add">
        <div class="inputData1">
            ФИО:
            <input type="text" name="familyName" size="22" placeholder="*Фамилия">
            <input type="text" name="firstName" size="22" placeholder="*Имя">
            <input type="text" name="middleName" size="23" placeholder="Отчество">
            Пол:
            <select name="sex">
                <option value="male">мужчина</option>
                <option value="female">женщина</option>
                <option value="nechto" selected="selected">infinitus</option>
            </select>
        </div>
        <div class="inputData2">
            phone:
            <input type="text" name="phone" size="12" placeholder="*+375-29-6000000">
            e-mail:
            <input type="text" name="email" size="16" placeholder="*user@provider.com">
            SkyPe:
            <input type="text" name="skype" size="15" placeholder="skaypiuskas">
            <input type="submit" value="✔ Добавить">
            <input type="reset" value="☐ Очистить">
            <div class="psText">p.s. поля с * обязательны к заполнению</div>
        </div>
    </form>
</div>
<?php
    // считываем данные из файла/базы
    $lData = file_get_contents('learners.dat');
    $lData = unserialize($lData);

    if (($lData != 'testDB') && (!count($lData)==0)) {
        // не тестовая база, выводим данные

        // показываем форму со списком (из файла) с переключателем редактирования/удаления
?>
<div class="listLearners">
    <div>
    список группы: 
    </div>
    <div class="formLearners">
    <form name="ListLearners" method="GET" action="learners_php.php">
       <input name="action" type="hidden" value="del">
<?php
        $frci = 0;
        for ($frci == 0; $frci < count($lData); $frci++) {
            // выводим в цикле весь список
            $lDataR=$lData[$frci];
            $familyName=$lDataR[0];
            $firstName =$lDataR[1];
            $middleName =$lDataR[2];
            viewLearnes($frci, $familyName, $firstName, $middleName); 
        }
?>
        <div class="buttons">
            <input type="submit" value="✘ Удалить"> подтверждение: <input type="checkbox" name="delete" value="yes"/>
        </div>        
    </form>
    </div>
</div>
<?php

    } else {
    echo 'список базы пуст.<br>';
    }

} elseif ($_GET['action'] == 'edit') {
    // редактируем элемент
    echo 'редактируем:';

} elseif ($_GET['action'] == 'add') {
    // проверяем правильность введенных данных
    if (isset($_GET['familyName']) 
    && isset($_GET['familyName']) 
    && isset($_GET['phone'])
    && isset($_GET['email'])
    && !filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
    ?>
    <div class="errorText">
    Есть важные незаполненные поля, или они заполнены не корректно!!!
    </div>      
    <input type="button" onclick="history.back()" value="Вернуться назад">
    <?php
    } else {
    // добавляем элемент
    echo 'добавляем:'. "\n";
    // считываем имеющиеся данные
    $lData = file_get_contents('learners.dat');
    $lData = unserialize($lData);
    if ($lData != 'testDB') {
        // считываем размер массива
        $nm = count($lData);
    } else {
        // первая запись
        $lData=array();
        $nm = 0;
        unlink('learners.dat');
    }
    $lData[] = array(0 => checkText($_GET['familyName']), checkText($_GET['firstName']), checkText($_GET['middleName']), checkText($_GET['sex']), checkText($_GET['phone']), checkText($_GET['email']), checkText($_GET['skype']));
    // выводим то что добавили
    echo '<pre>'. "\n";
    print_r ($lData[$nm]);
    echo '</pre>' . "\n";
    echo '<br><a href="learners_php.php?action=view" title="к списку">к списку</a><br><br>' . "\n";
    sort($lData); // отсортируем массив по порядку
    saveData($lData); // записываем в базу 
    }
} elseif (($_GET['action'] == 'del') && ((!isset($_GET['ldel'])) || ((isset($_GET['ldel']) && !isset($_GET['delete']) )))) {
?>
    <div class="errorText">
    Нужно выбрать удаляемую запись, выбрать "подтверждение" и нажать кнопку "удалить"
    </div>
    <input type="button" onclick="history.back()" value="Вернуться назад">
<?php
} elseif ($_GET['action'] == 'del' && isset($_GET['ldel']) && $_GET['delete'] == 'yes') {
    // удаляем элемент
    echo 'удалена запись:'. "\n";
    $lData = file_get_contents('learners.dat');
    $lData = unserialize($lData);
    // выводим что удалили
    echo '<pre>'. "\n";
    print_r ($lData[$_GET['ldel']]);
    echo '</pre>' . "\n";
    unset($lData[$_GET['ldel']]); // удаляем запись из массива
    sort($lData); // отсортируем массив по порядку
    echo '<br><a href="learners_php.php?action=view" title="к списку">к списку</a><br><br>' . "\n";
    saveData($lData); // записываем в базу
} elseif ((($_GET['action'] == 'view') && (!empty($_GET['nv']))) || ($_GET['nv']=='0')) {
    echo '<div class="viewData"><b>подробности записи:</b><br><br>' . "\n";
    $lData = file_get_contents('learners.dat');
    $lData = unserialize($lData);
    $lDataV=$lData[$_GET['nv']];
    if ($lDataV[3]=='male') {
        $sex='мужчина';
    } elseif ($lDataV[3]=='female') {
        $sex='женщина';
    } elseif ($lDataV[3]=='nechto') {
        $sex=':-\\'; // 
    }
    echo '  <div>ФИО: '.$lDataV[0].' '.$lDataV[1].' '.$lDataV[2].' ['.$sex.']'. "</div>\n";
    echo '  <div>телефон: '.$lDataV[4]. "</div>\n";
    echo '  <div>e-mail: '.$lDataV[5]. "</div>\n";
    if (isset($lDataV[6])) {
    echo '  <div>skype: '.$lDataV[6]. "</div>\n";
    }
    echo '</div>'. "\n";    
    echo '<br><a href="learners_php.php?action=view" title="к списку">к списку</a><br><br>' . "\n";
    echo '<!--'. "\n";
    print_r ($lData[$_GET['nv']]);
    echo '-->' . "\n";
}

// функции

// вывод элемента списка
function viewLearnes($ln, $familyName, $firstName, $middleName)
{
?>
        <div class="learner">
<?php
echo '            <input type="radio" name="ldel" value="' . $ln . '"/> ' . "\n";
echo '            ['.($ln+1).'] <a href="learners_php.php?action=view&nv='.$ln.'" title="подробно">'. $familyName . ' ' . $firstName . ' ' . $middleName . "</a>\n";
?>
        </div>
<?php
}

// сохранение данных в файл
function saveData($lData) {
    //записываем массив в файл
    $fData = fopen('learners.dat', 'w+');
    fwrite($fData, serialize($lData));
    fclose($fData);
}

// проверка введенных данных в формах
function checkText($text) {
    if (!empty($text))  {
        $text = trim($text);
        $text = stripslashes($text);
        $text = strip_tags($text);
        $text = htmlspecialchars($text);
      return $text;
    }
}
?>
</div>
</body>
</html>