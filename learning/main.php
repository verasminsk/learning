<?php
// переменные
$learner = 'Верещак Алексаднр Сергеевич';
$contract = '899-15';
$email = 'verasminsk@gmail.com';
$phone = '+375-29-6505558';
$skype = 'veras-pc';

// формируем страницу
echo '<html>' . "\n";
// шапка
echo '<head>' . "\n";
// свойства выводимого документа
echo '<meta http-equiv="content-type" content="text/html; charset=utf-8" />' . "\n";
// подключаем файл стилей
echo '<link rel="stylesheet" type="text/css" href="css/style.css" />' . "\n";
// базовый адрес страницы
//echo '<base href="http://corrino.no-ip.org/learning/">'."\n";
// заголовок страницы
echo '<title>welcome to corrino.no-ip.org</title>' . "\n";
echo '</head>' . "\n";

// тело страницы
echo '<body>' . "\n";
echo '<div id="container">' . "\n"; // контейнер всей страницы

echo '<div id="header">' . "\n"; // шапка страницы
echo '<div style="text-align: center;"><a href="http://corrino.no-ip.org/learning/" title="go to corrino.no-ip.org/learning/"><span style="text-decoration: underline;"><span style="font-size: x-small; ">Hello from laptop Corrino!</span></span><br></a></div><br>' . "\n";
echo '</div>' . "\n"; // id="header" шапка страницы

echo '<div id="content">' . "\n"; // id="content" содержание страницы
echo '<div style="text-align: center;"><h1>Программирование на PHP. Разработка WEB-приложений.</h1></div><br>' . "\n";

echo '<div id="learner">' . "\n"; // id="learner" данные ученика'."\n";
echo '<strong>' . $learner . ' </strong> [Договор №' . $contract . ']<br>
e-mail: ' . $email . '<br>
телефон: ' . $phone . '<br>
skype: ' . $skype . '<br>
<br>' . "\n";
echo '<br><a href="learning/learners_php.php" title="список с контактами">список группы</a><br><br>' . "\n";

echo '</div>' . "\n"; // id="learner" данные ученика страницы

echo '<div id="lessons">' . "\n"; // id="lessons" уроки
echo '<a href="../learning/lesson01.php" title="13.02.2016">Курсы PHP, урок №1</a><br>' . "\n";
echo '<a href="../learning/lesson02.php" title="20.02.2016">Курсы PHP, урок №2</a><br>' . "\n";
echo '<a href="../learning/lesson03.php" title="27.02.2016">Курсы PHP, урок №3</a><br>' . "\n";
echo '<a href="../learning/pontoon/" title="игра 21-но"</a>Игра 21-но.<br>' . "\n";
echo '<a href="../learning/lesson04.php" title="12.03.2016">Курсы PHP, урок №4</a><br>' . "\n";
echo '<a href="../learning/lesson05.php" title="20.03.2016">Курсы PHP, урок №5</a><br>' . "\n";
echo '<a href="../learning/pontoon2/" title="игра 21-но v2"</a>Игра 21-но. v2<br>' . "\n";
echo '<a href="../../" title="MVC"</a>MVC<br>' . "\n";

echo '</div>' . "\n"; // id="lessons" контейнер уроков
echo '</div>' . "\n"; // id="content" контейнер сдержания

echo '<div id="footer">' . "\n"; // футер страницы
echo '<div style="text-align: center;"><small style="text-decoration: underline;">&copy; VerAS</small></div>' . "\n";
echo '</div>' . "\n"; // id="footer" футер страницы

echo '</div>' . "\n"; // id="container" контейнер всей страницы
echo '</body>' . "\n";
echo '</html>' . "\n";
?>