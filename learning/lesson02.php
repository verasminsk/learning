<?php
// формируем страницу
echo '<html>' . "\n";
// шапка
echo '<head>' . "\n";
// свойства выводимого документа
echo '<meta http-equiv="content-type" content="text/html; charset=utf-8" />' . "\n";
// подключаем файл стилей
echo '<link rel="stylesheet" type="text/css" href="css/lesson.css" />' . "\n";

echo '<meta name="author" content="VrAS">';
echo '<meta name="description" content="описание сайта">';
echo '<meta name="keywords" content="основные слова-маркеры для поисковиков">';
// заголовок страницы
echo '<title>урок №2</title>' . "\n";
echo '</head>' . "\n";

// тело страницы
echo '<body>' . "\n";
echo '<div id="container">' . "\n"; // контейнер всей страницы

echo '<div id="header">' . "\n"; // шапка страницы
echo '<center>Урок №2 "back to the universe..."</center><br>' . "\n";
echo '</div>' . "\n"; // id="header" шапка страницы

echo '<div id="content">' . "\n"; // id="content" содержание страницы

// здесь сам урок
// разметка страницы fixed вверх строницы
//print_r ($_GET);
//print_r ($_POST);
/*echo '$_SERVER:<br>'.'<p>';
print_r ($_SERVER);
echo '<br>';
echo '<br>';
print_r ($_SERVER[SERVER_SOFTWARE]);
echo '</p>';*/

$_GET['ttest'] = 'get';
$_POST['ttest'] = 'post';


if (empty($_GET['nametext'])) {
    ?>
    <form name="nametext" method="GET" action="lesson02.php">
        Ваше имя:
        <input type="text" name="nametext" size="40" value="Александр">
        <input name="hidentext" type="hidden" value="скрытый передаваеммый текст">
        <br>
        <input type="radio" name="rdor" value="or1"/> или1
        <br>
        <input type="radio" name="rdor" value="or2"/> или2
        <br>
        a:
        <input type="checkbox" name="znc[]" value="a"/>
        <br>
        b:
        <input type="checkbox" name="znc[]" value="b"/>
        <br>
        c:
        <input type="checkbox" name="znc[]" value="c"/>
        <br>
        <select name="spis">
            <option value="q1">вариант1</option>
            <option value="q2">вариант2</option>
            <option value="q3" selected="selected">вариант3 по умолчанию</option>
            <option value="q4">вариант4</option>
        </select>
        <br>
        <input type="submit" value="Отправить">
        <input type="reset" value="Очистить">
    </form>
    <?php
} elseif (empty($_GET['rdor'])) {
    echo 'ошибка! нехватает данных "или1 или или2"<br>';
} elseif (!empty($_GET['rdor']) && (!file_exists('var.flag'))) {
    echo 'переданное имя методом GET <b>' . $_GET['nametext'] . '</b><br>';
    echo 'переданный скрытый текст методом GET <b>' . $_GET['hidentext'] . '</b><br>';
    echo 'переданное значение радиокнопки методом GET <b>' . $_GET['rdor'] . '</b><br>';
    echo 'переданное значение/я множетвенного выбора методом GET <b>' . $_GET['znc'] . '</b><br>';
    echo 'переданное значение выподающего списка методом GET <b>' . $_GET['spis'] . '</b><br>';
    // зписываем данные _GET в файл
    $v_flag = fopen('var.flag', "w+");
    fwrite($v_flag, serialize($_GET));
    fclose($v_flag);
} elseif (file_exists('var.flag')) {
// считываем из файла перменные
    $var_old = file_get_contents('var.flag');
    $var_old = unserialize($var_old);
    echo 'есть старый файл, считываем файл и удляем его<br>';
    print_r($var_old);
    unlink('var.flag');
} else {
    echo 'ошибка! <br>';
}
?>
<pre>
<?php
echo '<br>_POST <br>';
print_r($_POST);
echo '<br>_GET <br>';
print_r($_GET);
echo '<br>_REQUEST <br>';
print_r($_REQUEST);
?>
</pre>
<?php
echo '</div>' . "\n"; // id="content" содержание страницы
echo '<div id="footer">' . "\n"; // футер страницы
echo '<center><small><u>&copy; VerAS</u></small></center>' . "\n";
echo '</div>' . "\n"; // id="footer" футер страницы
echo '</div>' . "\n"; // id="container" контейнер всей страницы
echo '</body>' . "\n";
echo '</html>' . "\n";
?>
