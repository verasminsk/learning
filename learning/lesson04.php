<?php
header('Content-Type: text/html; charset=utf-8');
?>
    <html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="css/lesson.css"/>
        <title>урок №4</title>
    </head>
    <body>
    <h1>Урок №4</h1>
    <?php

    // работа с базами

    if (!isset($_GET['action'])
        || (($_GET['action'] == 'add') && ($_GET['name'] == ''))
        || (($_GET['action'] == 'addj') && ($_GET['genre'] == ''))
    ) {
        ?>
        <div>
            <form name="addBook" method="GET" action="lesson04.php">
                <input name="action" type="hidden" value="add">
                <div>
                    <input type="text" name="name" size="22" placeholder="*Название">
                    <input type="text" name="author" size="22" placeholder="*Автор">
                    <input type="text" name="description" size="23" placeholder="Описание">
                    <select name="genre">
                        <?php
                        // список жанров из таблицы жанров
                        ?>
                        <option value="genre1">жанр1</option>
                        <option value="genre2">жанр2</option>
                        <option value="genre3" selected="selected">жанр3</option>
                    </select>                </div>
                <input type="submit" value="Добавить Книгу">
                <input type="reset" value="Очистить">
            </form>
        </div>
        <div>
            <form name="addGenre" method="GET" action="lesson04.php">
                <input name="action" type="hidden" value="addj">
                <div>
                    <input type="text" name="genre" size="22" placeholder="*Жанр">
                </div>
                <input type="submit" value="Добавить Жанр">
                <input type="reset" value="Очистить">
            </form>
        </div>
        <div>
            <form name="view" method="GET" action="lesson04.php">
                <input name="action" type="hidden" value="view">
                 <input type="submit" value="Постмотреть список">
            </form>
        </div>
        <?php

    } elseif (($_GET['action'] == 'addj') && ($_GET['genre'] != '')) {
        //добавляем жанр
        $genre = $_GET['genre'];

        $db = connectDB(); // подключаемся к базе

        // записываем в базу
        $rows = $db->exec('INSERT INTO book_genre(name) VALUES ("' . $genre . '");');

        // в случае ошибки SQL выражения выведем сообщене об ошибке
        $error_array = $db->errorInfo();
        if ($db->errorCode() != 0000) {
            echo "SQL ошибка: " . $error_array[2] . '<br />';
        } else {
            echo 'ok<br>';
            echo '<input type="button" onclick="history.back()" value="Вернуться назад">';
        }

    } elseif ($_GET['action'] == 'add') {
// добавляем книгу
        $name = $_GET['name'];
        $author = $_GET['author'];
        $description = $_GET['description'];
        $genre = $_GET['genre'];
        $db = connectDB(); // подключаемся к базе

        // записываем в базу
        $rows = $db->exec('INSERT INTO books_catalog(name, author, description) VALUES ("' . $name . '", "' . $author . '", "' . $description . '");');

        // в случае ошибки SQL выражения выведем сообщене об ошибке
        $error_array = $db->errorInfo();
        if ($db->errorCode() != 0000) {
            echo "SQL ошибка: " . $error_array[2] . '<br />';
        } else {
            echo 'ok<br>';
            echo '<input type="button" onclick="history.back()" value="Вернуться назад">';
        }

    } elseif ($_GET['action'] == 'view') {
        echo 'просмотр таблицы книг';
        $db = connectDB(); // подключаемся к базе
        $data = $db -> prepare("SELECT * FROM books_catalog WHERE id=:id");
        if (!isset($_GET['id'])) {
            $id = $_GET['id'];
        } else {
            $id=14;
        }

        $data -> bindValue(':id',$id);
        $data -> execute();
        $row = $data -> fetch(PDO::FETCH_OBJ);
        var_dump($row);

    }

    ?>
    </body>
    </html>
<?php
function connectDB()
{
    $db = new PDO('mysql:host=localhost;dbname=db_learning;charset=UTF8', 'learner', 'pass',
        array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET \'utf8\'',
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SESSION collation_connection = \'utf8_general_ci\''));
    return $db;
}