<?php
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'viewcod') {
        echo '------------ index.php------------'."</br> \n";
        echo highlight_file('index.php', 'FALSE ')."</br> \n";
        echo '------------ playingCardsDeck.php ------------'."</br> \n";
        echo highlight_file('playingCardsDeck.php', 'FALSE ')."</br> \n";
        echo '------------ pontoon2.php------------'."</br> \n";
        echo highlight_file('pontoon2.php', 'FALSE ')."</br> \n";
        echo '------------ rules.php------------'."</br> \n";
        echo highlight_file('rules.php', 'FALSE ');
        die();
    }
}
?>
    <html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="css/pontoon.css"/>
        <meta name="author" content="VerAS">
        <meta name="description" content="игра в Двадцать одно v2">
        <meta name="keywords" content="игра играть двадцать одно 21 очко">
        <?php
        if (!isset($_GET['action']) || ($_GET['action'] == 'play')) {
            $title = 'Игра в двадцать одно v2.';
        } elseif ($_GET['action'] == 'rules') {
            $title = 'Правила игры в двадцать одно.';
        } elseif ($_GET['action'] == 'stat') {
            $title = 'Статистика игры в двадцать одно.';
        } else {
            $title = 'Игра в двадцать одно';
        }
        echo '<title>' . $title . '</title>' . "\n";
        ?>
    </head>
    <body>
    <div id="content">
        <?php
        if (!isset($_GET['action'])) {
            // стартовая страница
            echo '<div class="header">Здесь Вы можете сыграть в игру "Двадцать одно". v2</div>' . "\n";
            echo '<div class="butt"><a href="?action=play" title="играть">Играть</a></div>' . "\n";
            echo '<div class="butt"><a href="?action=stat" title="статистика иры">Статистика игры</a></div>' . "\n";
            echo '<div class="butt"><a href="?action=rules" title="правила игры в двадцать одно">Правила игры</a></div>' . "\n";
            echo '<div class="butt"><a href="?action=viewcod" title="код игры">Смотреть код игры</a></div>' . "\n";
        } elseif ($_GET['action'] == 'play') {
            // игра
            echo '<div class="header">Игра в двадцать одно. v2</div>' . "\n";
            $koloda = new playingCardsDeck();

            if (!isset($_GET['koloda'])) {
                // начало игры раздаем по очереди карты игрокам
                $koloda->GiveCard('1');
                $koloda->GiveCard('2');
                $koloda->GiveCard('1');
                $koloda->GiveCard('2');
            } else {
                //продолжаем игру, сдаем карту игроку
                $goPlay = $_GET['goPlay'];
                $koloda = unserialize($_GET['koloda']); // восстанавливаем объект
                $koloda->GiveCard($goPlay); // карту игроку
            }

            chekPlaySum($koloda->sumCardPlayer1, '1', count($koloda->cardPlayer1));
            chekPlaySum($koloda->sumCardPlayer2, '2', count($koloda->cardPlayer2));

            viewKardPlayer($koloda->cardPlayer1, '1', $koloda->sumCardPlayer1);
            viewKardPlayer($koloda->cardPlayer2, '2', $koloda->sumCardPlayer2);

            if (($koloda->sumCardPlayer1 >= 21) || ($koloda->sumCardPlayer2 >= 21)) {
                $koloda = NULL;
                // игра закончена
                echo '<div class="butt"><a href="?action=play" title="играть">Играть ещё.</a></div>' . "\n";
                echo '<div class="butt"><a href="?action=stat" title="статистика иры">Статистика игры</a></div>' . "\n";
                echo '<div class="butt"><a href="?action=viewcod" title="код игры">Смотреть код игры</a></div>' . "\n";
            } else {
                // предлагаем еще по карте
                echo '<div id="control">';
                formAddcard($koloda, '1');
                formAddcard($koloda, '2');
                echo '<div class="butt"><a href="?action=rules" title="правила игры в двадцать одно">Правила игры</a></div>' . "\n";
                echo '</div>';
            }
        } elseif ($_GET['action'] == 'rules') {
            // описание правил игры
            echo '<div class="header">Описание правил Игры в двадцать одно.</div>' . "\n";
            require_once('rules.php'); // правила во внешнем файле
        } elseif ($_GET['action'] == 'stat') {
            // вывод статистики результатов игры
            echo '<div class="header">Статистика Игры в двадцать одно.</div>' . "\n";
            echo 'секретно, пока....';
            echo '<div class="butt"><a href="?action=play" title="играть">Играть ещё.</a></div>' . "\n";
        } else {
            // хм.... левый action
            echo '<div class="errorText">' . "\n";
            echo '<div class="header">Таки добрый дзень, кулхацкер...</div>' . "\n";
            echo '</div>' . "\n";
            echo '<div class="butt"><a href="?action=play" title="играть">Играть</a></div>' . "\n";
            echo '<div class="butt"><a href="?action=stat" title="статистика иры">Статистика игры</a></div>' . "\n";
            echo '<div class="butt"><a href="?action=rules" title="правила игры в двадцать одно">Правила игры</a></div>' . "\n";
            echo '<div class="butt"><a href="?action=viewcod" title="код игры">Смотреть код игры</a></div>' . "\n";
        }
        ?>
    </div>
    </body>
    </html>
<?php

function viewKardPlayer($card, $payer, $playSum)
{
    echo '<div id="player' . $payer . '">' . "\n";
    echo '<div>карты ' . $payer . ' игрока: </div>' . "\n";
    $kk = 0;
    foreach ($card as $key => $ct) {
        $kk++;
        echo '<div class="card' . ($kk) . '">';
        if (substr($key, 0, 1) == 'p') {
            echo '<div class="piki">' . mb_substr($key, 1) . ' &#9824;</div>';
        } elseif (substr($key, 0, 1) == 'k') {
            echo '<div class="kresti">' . mb_substr($key, 1) . ' &#9827;</div>';
        } elseif (substr($key, 0, 1) == 'b') {
            echo '<div class="bubni">' . mb_substr($key, 1) . ' &#9830;</div>';
        } elseif (substr($key, 0, 1) == 'c') {
            echo '<div class="cervi">' . mb_substr($key, 1) . ' &#9829;</div>';
        }
        echo '</div>' . "\n";;
    }
    echo '<div class="sumCard">сумма карт ' . $payer . ' игрока: ' . $playSum . "</div>\n";
    echo '</div>' . "\n";
}

function formAddcard($koloda, $player)
{
    if ($player == 1) {
        $napr = '<-';
    } else {
        $napr = '->';
    }
    echo '<form name="addKart' . $player . '" method="GET" action="?">' . "\n";
    echo '       <input name="action" type="hidden" value="play">' . "\n";
    echo '       <input name="goPlay" type="hidden" value="' . $player . '">' . "\n";
    echo '       <input name="koloda" type="hidden" value=\'' . serialize($koloda) . '\'>' . "\n";
    echo '<input type="submit" value="' . $napr . ' Еще карту ' . $player . '-му?">' . "\n";
    echo '</form>' . "\n";
}

function chekPlaySum($playSum, $player, $cKart)
{
    if (($playSum == 22) && ($cKart == 2)) {
        echo '<div class="victory">Победа ' . $player . ' игрока - Золотое ОЧКО!!!</div>' . "\n";
        saveDBrz($playSum, $player, '1', '0');
    } elseif ($playSum == 21) {
        echo '<div class="victory">Победа ' . $player . ' игрока - 21-но !!!</div>' . "\n";
        saveDBrz($playSum, $player, '1', '0');
    } elseif ($playSum > 21) {
        echo '<div class="losing">' . $player . ' игрок ПРОИГРАЛ!!!</div>' . "\n";
        saveDBrz($playSum, $player, '0', '1');
    }
}

function saveDBrz($playSum, $player, $victory, $losing)
{
    // сохраняем результат игры в базу данных
    $db = connectDB(); // подключаемся к базе
    // записываем в базу
    $rows = $db->exec('INSERT INTO pontoon(player, victory, losing, score) VALUES ("' . $player . '", "' . $victory . '", "' . $losing . '", "' . $playSum . '");');
    // в случае ошибки SQL выражения выведем сообщене об ошибке
    $error_array = $db->errorInfo();
    if ($db->errorCode() != 0000) {
        echo "SQL ошибка: " . $error_array[2] . '<br />' . "\n";
    }
}

function connectDB()
{
    $db = new PDO('mysql:host=localhost;dbname=db_pontoon;charset=UTF8', 'pontoon', 'pontoon',
        array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET \'utf8\'',
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SESSION collation_connection = \'utf8_general_ci\''));
    return $db;
}

?>