<?php

class playingCardsDeck
{
    public $koloda = array(); // ассоциативный массив "карта":"стоимость"
    public $cardPlayer1 = array(); // ассоциативный массив "карта":"стоимость"
    public $cardPlayer2 = array(); // ассоциативный массив "карта":"стоимость"
    public $sumCardPlayer1; // сумма карт первого игрока
    public $sumCardPlayer2; // сумма карт второго игрока

    public function __construct()
    {
        if (!isset($_GET['koloda'])) {
            // создаем колоду карт - массив из колоды в 36 карт
            // ассоциативный массив "карта":"стоимость"
            // p-пики, k-крести, b-бубны, с-черви
            $masty = array(0 => 'p', 'k', 'b', 'c');
            foreach ($masty as $m) {
                for ($k = 6; $k <= 10; $k++) {
                    // карты от 6 до 10 по своему номиналу
                    $this->koloda[$m . $k] = $k;
                }
                $this->koloda[$m . 'V'] = 2; // валет
                $this->koloda[$m . 'D'] = 3; // дама
                $this->koloda[$m . 'K'] = 4; // король
                $this->koloda[$m . 'T'] = 11; // туз
            }
        } else {
            $this->CalcSumCard('1');
            $this->CalcSumCard('2');
        }
    }

    public function GiveCard($player)
    {
        // дать карту игроку из колоды
        $randCard = array_rand($this->koloda, 1); // случайная одна карта из колоды
        if ($player == '1') {
            $this->cardPlayer1[$randCard] = $this->koloda[$randCard];
        } elseif ($player == '2') {
            $this->cardPlayer2[$randCard] = $this->koloda[$randCard];
        }
        unset($this->koloda[$randCard]); // удаляем её из колоды
        playingCardsDeck::CalcSumCard($player);
    }

    public function CalcSumCard($player)
    {
        // считаем сумму карт игрока
        if ($player == '1') {
            $this->sumCardPlayer1 = 0;
            foreach ($this->cardPlayer1 as $cardP_1) {
                $this->sumCardPlayer1 = $this->sumCardPlayer1 + $cardP_1;
            }
        } elseif ($player == '2') {
            $this->sumCardPlayer2 = 0;
            foreach ($this->cardPlayer2 as $cardP_2) {
                $this->sumCardPlayer2 = $this->sumCardPlayer2 + $cardP_2;
            }
        }
    }
}

?>