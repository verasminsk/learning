<?php
namespace TestMVC\Core;

class Template
{
    public $layout;
    protected $tplVars = [];
    protected $tplName;
    private $bodyTplFile;
    
    public function __construct($tplName = '', $layoutName = '')
    {
        if (empty($layoutName)) {
            $this->layout = \TestMVC\Core\Config::getAppPath() . '/Views/Layout/default.php';
        } else {
            $this->layout = $layoutName;
        }

        if (!empty($tplName)) {
            $this->tplName = $tplName;
        }
        
    }

    public function setLayout($name)
    {
        $this->layout = $name;
    }

    public function setVarInTemplate($name, $value)
    {
        $this->tplVars[$name] = $value;
    }

    public function setTplName($tplName)
    {
        $this->tplName = $tplName;
    }

    public function render($needLayout = true)
    {
        extract($this->tplVars);
        $this->bodyTplFile = \TestMVC\Core\Config::getAppPath().'/Views/'.$this->tplName.'.php';
        if($needLayout) {
         if (file_exists($this->layout) && file_exists($this->bodyTplFile)) {
             require($this->layout);
         } else {
             throw new \Exception('Layout or body tpl is not found!', 500);
         }
        } else {

           if (file_exists($this->bodyTplFile)) {
             require($this->bodyTplFile);
         } else {
             throw new \Exception('body tpl is not found!', 500);
         }
        }
    }

    public function __set($name, $value)
    {
        $this->tplVars[$name] = $value;
    }

    public function __get($name)
    {
        if (!empty($this->tplVars[$name])) {
            $result = $this->tplVars[$name];
        } else {
            $result = null;
        }

        return $result;
    }
}