<?php
namespace TestMVC\Core; // к чему относиться наш класс

class Router
{
    // класс управления загрузки контента по строке урла
    private static $instance; // приватная, сам объект (маршрутизатор проекта)
    public $currentController; // текущийКонтроллер
    public $currentAction; // текущееДействие

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            // объект еще не создан
            self::$instance = new self(); // создаем объект
        }
        return self::$instance; // возвращаем ссылку на объект
    }

    private function __construct()
    {
        //объект должен быть только один и созданный внутри объекта используя getInstance
    }

    // функция маршрутизации
    public function routeRequest()
    {
        if (!empty($_GET['r'])) {
            // есть преобразованный урл
            // в .htaccess есть "RewriteRule ^(.*)$ index.php?r=$1 [L,QSA]" -  передать в $_GET['r'] урл запроса
            $route = $_REQUEST['r']; // "путь" нашего маршрута
            $path = explode('/', $route); // разделяем по "/" и создаем массив из элементов
            // есть косяк - при передаче только контроллера без действия будет ошибка "Unknown error type: [8] Undefined offset: 1"
            // *** добавим проверку, если не два элемента то кривой урл404
            if (count($path)===2) {
                $controller = ucfirst($path[0]); // первый элемент контроллер
                $action = $path[1]; // второй элемент массива действие
            } else if (count($path) == 1) { //Хорошей практикой считается иметь некий метод по умолчанию, к-й вызовется если нету конкретного указанного
               $controller = ucfirst($path[0]); // первый элемент контроллер
               $action = 'index';  //в каждом контроллере может быть метод index - метод по умолчанию
            } else {
                $this->handle404(); // нет такой страницы
                return; // прерываем выполнение функции
            }
        } else {
            // выводим главную страницу
            $controller = 'Main'; // контроллер главной страницы
            $action = 'Index'; // действие контроллера Index
        }

        $this->currentController = $controller; // создаем ссылку на публичную переменную currentController нашего объекта
        $controller = '\TestMVC\Application\Controllers\\' . $controller . 'Controller'; // "родственная связь", полный виртуальный путь к контроллеру
        if (class_exists($controller)) {
            // если существует  запрошенный класс
            if (method_exists($controller, $action . 'Action')) {
                // если есть в нашем классе запрашиваемый метод
                $this->currentAction = lcfirst($action); // на всякий случай первый символ переводим в нижний регистр
                $action = $action . 'Action'; // приводим запрошенное действие к стандарту
            } else {
                // нет в кассе такого действия
                $this->handle404(); // выводим ошибку 404 (нет страницы)
            }
        } else {
            // нет такого класса
            $this->handle404(); // выводим ошибку 404 (нет страницы)
        }

        $controller = new $controller(); // создаем объект контроллера запрошенной страницы
        $controller->{$action}(); // выполняем запрошенное действие
    }

    // функция сообщения о ненайденной странице
    public function handle404()
    {
        $controller = new \TestMVC\Application\Controllers\ErrorController(); // создаем объект обработки ошибок
        $controller->error404Action(); // выводим ошибку 404 (нет страницы)
    }
}