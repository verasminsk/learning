<?php
namespace TestMVC\Core;

class DB implements TestInterface
{
    public static $instance = false;
    private $host;
    private $user;
    private $pass;
    private $dbType;
    private $dbName;
    private $pdo;

    public function testMethod()
    {

    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->host = \TestMVC\Core\Config::get('db.host');
        $this->user = \TestMVC\Core\Config::get('db.user');
        $this->pass = \TestMVC\Core\Config::get('db.pass');
        $this->dbName = \TestMVC\Core\Config::get('db.dbName');
        $this->dbType = \TestMVC\Core\Config::get('db.dbType');
    }

    public function connect()
    {
        $dsn = $this->dbType . ":dbname=" . $this->dbName . ";host=" . $this->host;
        try {
            $this->pdo = new \PDO($dsn, $this->user, $this->pass,
                array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function query($sql, $params = [])
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt;
    }


}