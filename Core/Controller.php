<?php
namespace TestMVC\Core;

class Controller
{
    public $template;
    public $config;

    public function __construct()
    {
        
        $this->template = new Template();
        $this->template->setVarInTemplate('author', Config::get('meta.author'));
        $this->template->setVarInTemplate('description', Config::get('meta.description'));
        $this->template->setVarInTemplate('title', Config::get('meta.title'));
        $this->template->setTplName(Router::getInstance()->currentController . '/' . Router::getInstance()->currentAction);
    }
    
    public function isAjax()
    {
      return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}