<?php
ini_set('display_errors', 1); // включаем отображение ошибок на экран
require_once('Core/Autoloader.php'); // подгружаем автолоадер/автозагрузчик классов из файлов
\TestMVC\Core\Autoloader::init(); // инициализация автозагрузчика
$config = \TestMVC\Core\Config::getInstance(); // создание объекта конфигурации (get instance - получить экземпляр объекта)
$config->init(); // команда объекту конфигурации -> подгрузить конфиги из файлов
$db = \TestMVC\Core\DB::getInstance(); // получаем объект базы данных
$db->connect(); // выполняем подключение к базе
$errorController = new \TestMVC\Application\Controllers\ErrorController(); // создаем объект обработки
set_error_handler(array($errorController,'handleError')); // устанавливаем свой обработчик ошибок
set_exception_handler(array($errorController,'handleError')); // устанавливаем свой обработчик исключений
$router = \TestMVC\Core\Router::getInstance(); // создаем объект обработки запросов пользователя (обработка адресной строки)
$router->routeRequest(); // обрабатываем строку запроса
